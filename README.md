# Point Occlusion

I built this application at the request of a colleague doing SLAM research. This is a quick OpenGL program showing how to render shapes using GL_POINTS but still having solid-geometry occlusion of those shapes. Also shows how to individually ID the points (by pixel colour) that are not occluded by other geometry; each point has a unique ID/colour. Use the mouse (click-and-drag) to look around, and the mouse wheel to zoom in and out.

Built for Linux only, but there's no reason it couldn't be compiled under Windows or another operating system (although anything that uses GLES, like the Raspberry Pi, would require some minor modifications).

This example program uses old-school OpenGL stuff, such as client state arrays. While this is not as bad as something like immediate mode, this is still not as good as using vertex array objects. This decision was made because it is a reasonable middle ground between simplicity, performance, and the sanity of those for whom this example application was written.

## Dependencies

OpenGL, [GLFW3](http://www.glfw.org/), [GLEW](http://glew.sourceforge.net/), and [GLM](http://glm.g-truc.net/0.9.8/index.html) (already included locally in src/3rdparty/glm).

## Running

Both the debug and release executables are included in the root directory. Use the the mouse (click-and-drag) to rotate, and the mouse wheel to zoom.

## Compiling

Executable is already included, but if you want to make modifications and build it, you'll need Code::Blocks. If you don't have it already, you can download Code::Blocks from [here](http://codeblocks.org/downloads/26).

## Contact

Feel free to contact Geoff at geoff.nagy@gmail.com if you have any questions.

