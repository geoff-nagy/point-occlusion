// Point Occlusion
// Geoff Nagy
// A quick program showing how to render shapes using GL_POINTS but still having solid-geometry occlusion. ALso
// shows how to individually ID the points (by pixel colour) that are not occluded by other geometry.
// Use the mouse (click-and-drag) to look around, and the mouse wheel to zoom in and out.

/*
Some notes:

- This example program uses old-school OpenGL stuff, such as client state arrays. While this is not as bad as something
  like immediate mode, this is still not as good as using vertex array objects. This decision was made because it is a
  reasonable middle ground between simplicity and understanding.

- Due to imprecisions in the depth buffer, it is necessary to scale up the point-based geometry slightly to ensure that
  occlusion doesn't occur when it shouldn't (i.e., when a point is sitting directly on the face of a primitive); there's
  really no way around this as far as I know (even with glDepthFunc(GL_LEQUAL)), but it shouldn't present a problem.
*/

#include <GL/glew.h>								// OpenGL extension wrangler, otherwise we only get GL 1.1 or so
#include <GLFW/glfw3.h>								// context, window, and input handling

#include "gldebugging/gldebugging.h"				// extremely useful GL callback debugging

#include "glm/glm.hpp"								// OpenGL Mathematics library to handle our vector and matrix math
#include "glm/gtc/matrix_transform.hpp"				// for lookAt() and perspective(), since we shouldn't use GLU for these (GLU is deprecated)
#include "glm/gtc/type_ptr.hpp"						// for value_ptr() when accessing matrices as floating-point arrays
#include "glm/gtx/rotate_vector.hpp"				// used to compute camera viewing angles when rotating around the center of the world
using namespace glm;

#include <cstdlib>
#include <iostream>
#include <vector>
using namespace std;

// GLFW window and characteristics
GLFWwindow *window;
vec2 windowSize;

// framebuffer rendering objects and textures
GLuint sceneFBO;
GLuint sceneColorTexture;
GLuint sceneDepthTexture;
GLuint sceneDepthBuffer;

// camera position
float cameraDistance, cameraAngleX, cameraAngleY;
vec3 cameraPos;

// track old mouse position for mouse control of camera
double oldMouseX, oldMouseY;

// camera matrices that we use when rendering; the projection matrix defines how geometry looks
// to the camera (perspective, orthographic, etc.), while the view matrix defines where the camera is
mat4 perspectiveProjection;
mat4 perspectiveView;
mat4 orthoProjection;							// ortho view matrix not required, since the ortho camera doesn't move around

// sphere geometry data
GLfloat *sphereSolidVertices;					// vertex list for solid sphere
GLushort *sphereSolidIndices;					// this list indexes into sphereSolidVertices when rendering the actual sphere
GLfloat *spherePointVertices;					// this is a list of the points on the sphere we want to render
int numSphereSolidVertices;						// these three vars correspond to the number of elements in each of the above arrays
int numSphereSolidIndices;
int numSpherePointVertices;

// cube geometry data
GLfloat *cubeSolidVertices;						// vertex list for solid cube
GLushort *cubeSolidIndices;						// this list indexes into cubeSolidVertices when rendering the actual cube
GLfloat *cubePointVertices;						// this is a list of the points on the cube we want to render
int numCubeSolidVertices;						// these three vars correspond to the number of elements in each of the above arrays
int numCubeSolidIndices;
int numCubePointVertices;

// for tracking the IDs of the points we render
unsigned int *idList;
unsigned int *idListPtr;

// quad geometry data (the quad is only used to render the final scene result, which is rendered to a texture rather
// than to the backbuffer)
GLfloat *quadVertices;
GLfloat *quadTexCoords;
int numQuadVertices;

// window/GL intialization functions
void openWindow();
void prepareOpenGL();
void prepareFramebufferRendering();
void prepareViewingParams();

// camera control
void mouseScrollCallback(GLFWwindow* window, double x, double y);
void moveCameraWithMouse();
void setCameraPosition(float distance, float angleX, float angleY);

// sphere setup and rendering
void initSphere(int rings, int sectors);
void renderSphereSolid();
void renderSpherePoints();

// cube setup and rendering
void initCube(int sectors);
void renderCubeSolid();
void renderCubePoints();

// quad setup and rendering
void initQuad();
void renderQuad(GLuint texture);

// - - - main program - - - //

int main(int args, char *argv[])
{
    // settings for our sphere geometry; change these as you please
    const int SPHERE_RINGS = 24;
    const int SPHERE_SECTORS = 48;

    // settings for cube geometry; change these as you please
	const int CUBE_SECTORS = 12;

	// this is necessary to help avoid points being incorrectly occluded by the solid
	// geometry due to imprecisions in the depth buffer
	const float POINT_SCALE_OFFSET = 0.04;

	// we request the contents of our rendered scene texture in RGBRGBRGB format, with each R, G, or B as a single unsigned byte
	unsigned char *pixelData;
	unsigned char *pixelPtr;
	unsigned int numPixels;
	unsigned int r, g, b;
	unsigned int id;				// will throw "unused var" warning if IDing is commented out below
	unsigned int i;

	// create our OpenGL window and context, and set some rendering options
	openWindow();
	prepareOpenGL();
	prepareFramebufferRendering();
	prepareViewingParams();

	// now that we know our window size, create space for our pixel data
	numPixels = windowSize.x * windowSize.y;
	pixelData = new unsigned char[numPixels * 3];

	// prepare our ID list---importantly, 0xffffff does not count as a value ID, because this is white (our background)
	idList = new unsigned int[256 * 256 * 256 - 1];
	idListPtr = idList;
	for(i = 0; i < 0x00ffffff; i ++)		// we go up to A: 0, B: 255, G: 255, R: 255;
	{										// we don't bother with the left-most byte because it is
        (*idListPtr++) = i;					// an alpha value that doesn't (SHOULDN'T!) affect the point colour
	}

	// prepare our shit
	initSphere(SPHERE_RINGS, SPHERE_SECTORS);
	initCube(CUBE_SECTORS);
	initQuad();

	// we exit if the user presses the 'X' or the ESC key
	while(!glfwWindowShouldClose(window) && !glfwGetKey(window, GLFW_KEY_ESCAPE))
	{
		// prepare to render everything to the scene texture; result goes in sceneColorTexture
		glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);
		glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);
		glDrawBuffer(GL_COLOR_ATTACHMENT0);

		// update the camera position from mouse input
		moveCameraWithMouse();
		setCameraPosition(cameraDistance, cameraAngleX, cameraAngleY);

		// reset the point IDs tracker
		idListPtr = idList;

        // step 1: render our solid geometry as white; we won't see it, but the depth buffer will get some values in it
        glDepthFunc(GL_LESS);
        glColor4f(1.0, 1.0, 1.0, 1.0);
        glPushMatrix();								// sphere #1
            glTranslatef(0.0, 2.0, 0.0);
            glScalef(5.0, 5.0, 5.0);
            renderSphereSolid();
        glPopMatrix();
        glPushMatrix();								// sphere #2
            glTranslatef(2.0, -2.0, 0.0);
            glScalef(7.0, 2.0, 7.0);
            renderSphereSolid();
        glPopMatrix();
        glPushMatrix();								// cube #1
            glTranslatef(-5.0, -2.0, 0.0);
            glScalef(7.0, 7.0, 7.0);
            renderCubeSolid();
        glPopMatrix();

        // step 2: render the points-only versions of our geometry; we will see these points, but only the portions
        // that have not already been obscured by our solid geometry
        glDepthFunc(GL_LEQUAL);
        glColor4f(1.0, 1.0, 1.0, 1.0);
        //glPointSize(2.0);
        glPushMatrix();								// sphere #1
            glTranslatef(0.0, 2.0, 0.0);
            glScalef(5.0 + POINT_SCALE_OFFSET, 5.0 + POINT_SCALE_OFFSET, 5.0 + POINT_SCALE_OFFSET);
            renderSpherePoints();
        glPopMatrix();
        glPushMatrix();								// sphere #2
            glTranslatef(2.0, -2.0, 0.0);
            glScalef(7.0 + POINT_SCALE_OFFSET, 2.0 + POINT_SCALE_OFFSET, 7.0 + POINT_SCALE_OFFSET);
            renderSpherePoints();
        glPopMatrix();
        glPushMatrix();								// cube #1
            glTranslatef(-5.0, -2.0, 0.0);
            glScalef(7.0 + POINT_SCALE_OFFSET, 7.0 + POINT_SCALE_OFFSET, 7.0 + POINT_SCALE_OFFSET);
            renderCubePoints();
        glPopMatrix();

        // step 3: at this point, our scene has been rendered to a texture, and we can copy the data from that
        // texture into a buffer that we can then search through for individual point IDs
        glBindTexture(GL_TEXTURE_2D, sceneColorTexture);
        glGetTexImage(GL_TEXTURE_2D, 0, GL_RGB, GL_UNSIGNED_BYTE, pixelData);		// this is a slow function!
        pixelPtr = pixelData;
        i = 0;
        while(i < numPixels)
        {
			// pull the ID from the R, G, B values we read from the current pixel
			r = *(pixelPtr + 0);
			g = *(pixelPtr + 1);
			b = *(pixelPtr + 2);
			id = (r + (g << 8) + (b << 16));

			// an ID of 0x00ffffff means there is no point rendered there; uncomment this section to see
			// the list of points we found; IMPORTANTLY, this loop runs fast enough (i.e., at interactive
			// rates) when this section below is uncommented, since it's std output that makes this loop
			// slow, not the iteration itself
			/*
			if(id != (unsigned int)0x00ffffff)
			{
				cout << "Found ID " << id << endl;
				cout << "\tcolor: " << r << ", " << g << ", " << b << endl;
			}*/
			i ++;
			pixelPtr += 3;
        }

        // step 4: because our scene has been rendered to a texture, we can't see it; so, we're going to render
        // that texture to a quad that fills the entire screen

        // turn off framebuffer rendering and clear the back buffer
		glBindFramebuffer(GL_FRAMEBUFFER, 0);
        glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);

		// invoke our orthographic projection for displaying the quad
		glMatrixMode(GL_PROJECTION);
		glLoadIdentity();
		glLoadMatrixf((GLfloat*)value_ptr(orthoProjection));

		// restore modelview matrix mode and set to the identity
		glMatrixMode(GL_MODELVIEW);
		glLoadIdentity();

        // render the quad with the texture
        glColor4f(1.0, 1.0, 1.0, 1.0);
        glPushMatrix();
			glScalef(windowSize.x, windowSize.y, 1.0);
			renderQuad(sceneColorTexture);
		glPopMatrix();

		// get input events and swap the back buffer with the front buffer
		glfwPollEvents();
		glfwSwapBuffers(window);
	}

	// shut down GLFW
	glfwDestroyWindow(window);
	glfwTerminate();
}

// - - - geometry preparation - - - //

// thanks, StackOverflow! Adapted from:
// https://stackoverflow.com/questions/7946770/calculating-a-sphere-in-opengl
void initSphere(int rings, int sectors)
{
    const float R = 1.0 / (float)(rings - 1);
    const float S = 1.0 / (float)(sectors - 1);

    GLfloat *vertexPtr;                     // for iteration through vertex...
    GLushort *indexPtr;						// ...and index lists
    int p1, p2, p3, p4;						// for indexing into vertex list to build average quad positions
    float x, y, z;                          // coordinates of each vertex as we build it
    int r, s;                               // iterators for rings and sectors

    // build the list of vertices to use in our solid sphere
    numSphereSolidVertices = rings * sectors;
    sphereSolidVertices = new GLfloat[numSphereSolidVertices * 3];
    vertexPtr = sphereSolidVertices;
    for(r = 0; r < rings; r++)
    {
        for(s = 0; s < sectors; s++)
        {
            y = sin(-M_PI_2 + M_PI * r * R);
            x = cos(2 * M_PI * s * S) * sin(M_PI * r * R);
            z = sin(2 * M_PI * s * S) * sin(M_PI * r * R);

            (*vertexPtr++) = x;
            (*vertexPtr++) = y;
            (*vertexPtr++) = z;
        }
    }

    // this defines how we index into the vertex list we just created, when rendering the sphere
    numSphereSolidIndices = (rings - 1) * (sectors - 1) * 4;
    sphereSolidIndices = new GLushort[numSphereSolidIndices];
    indexPtr = sphereSolidIndices;
    for(r = 0; r < rings - 1; r++)
    {
        for(s = 0; s < sectors - 1; s++)
        {
            (*indexPtr++) = r * sectors + s;
            (*indexPtr++) = r * sectors + (s + 1);
            (*indexPtr++) = (r + 1) * sectors + (s + 1);
            (*indexPtr++) = (r + 1) * sectors + s;
        }
    }

    // now; we know where our quads are, so let's use our index list to get average positions of all four vertices
    // of every one of those quads in our sphere---this gives us a list of points appearing in the middle of each quad
    numSpherePointVertices = (rings - 1) * (sectors - 1);
    spherePointVertices = new GLfloat[numSpherePointVertices * 3];
    vertexPtr = spherePointVertices;
    for(r = 0; r < rings - 1; r++)
    {
        for(s = 0; s < sectors - 1; s++)
        {
			// we have to use the same indices we used when computing the index list
            p1 = (r * sectors + s) * 3;
            p2 = (r * sectors + (s + 1)) * 3;
            p3 = ((r + 1) * sectors + (s + 1)) * 3;
            p4 = ((r + 1) * sectors + s) * 3;

            (*vertexPtr++) = (sphereSolidVertices[p1 + 0] +
                              sphereSolidVertices[p2 + 0] +
                              sphereSolidVertices[p3 + 0] +
                              sphereSolidVertices[p4 + 0]) / 4.0;			// x coord

            (*vertexPtr++) = (sphereSolidVertices[p1 + 1] +
                              sphereSolidVertices[p2 + 1] +
                              sphereSolidVertices[p3 + 1] +
                              sphereSolidVertices[p4 + 1]) / 4.0;			// y coord

            (*vertexPtr++) = (sphereSolidVertices[p1 + 2] +
                              sphereSolidVertices[p2 + 2] +
                              sphereSolidVertices[p3 + 2] +
                              sphereSolidVertices[p4 + 2]) / 4.0;			// z coord
        }
    }
}

// we will use the fancy triangle-strip approach outline in http://www.cs.umd.edu/gvil/papers/av_ts.pdf
// to do this---rendering a cube with a triangle strip is way cooler than regular separate triangles
void initCube(int sectors)
{
	const GLfloat VERTICES[] = {-0.5, -0.5, 0.5,		// 0 front half
								-0.5, 0.5, 0.5,			// 1
								0.5, 0.5, 0.5,			// 2
								0.5, -0.5, 0.5,			// 3
								-0.5, -0.5, -0.5,		// 4 back half
								-0.5, 0.5, -0.5,		// 5
								0.5, 0.5, -0.5,			// 6
								0.5, -0.5, -0.5};		// 7
	const GLushort INDICES[] = {1, 2, 0, 3, 7, 2, 6, 1, 5, 0, 4, 7, 5, 6};

	GLfloat *vertexPtr;
	int x, y, z;

	// copy the vertex list into dynamically allocated memory
	numCubeSolidVertices = 8;
	cubeSolidVertices = new GLfloat[numCubeSolidVertices * 3];
	memcpy(cubeSolidVertices, VERTICES, sizeof(GLfloat) * numCubeSolidVertices * 3);

	// do the same with the index list
	numCubeSolidIndices = 14;
	cubeSolidIndices = new GLushort[numCubeSolidIndices];
	memcpy(cubeSolidIndices, INDICES, sizeof(GLushort) * numCubeSolidIndices);

	// now, for the points that surround the cube...
	// this is not the most efficient way to put points on the outside of the cube, but it's
	// short to write, easy to understand, and not horribly awful
	numCubePointVertices = (((sectors + 1) * (sectors + 1)) * 2) +			// front and back +
							((sectors * 4) * (sectors - 1)); 				// inner rings
	cubePointVertices = new GLfloat[numCubePointVertices * 3];
	vertexPtr = cubePointVertices;
	for(x = 0; x < sectors + 1; x ++)
	{
		for(y = 0; y < sectors + 1; y ++)
		{
			for(z = 0; z < sectors + 1; z ++)
			{
				// only fill in outer portions
				if(x == 0 || x == sectors ||
				   y == 0 || y == sectors ||
				   z == 0 || z == sectors)
				{
					(*vertexPtr++) = ((float)x / (float)sectors) - 0.5;
					(*vertexPtr++) = ((float)y / (float)sectors) - 0.5;
					(*vertexPtr++) = ((float)z / (float)sectors) - 0.5;
				}
			}
		}
	}
}

void initQuad()
{
	const GLfloat VERTICES[] = {0.0, 0.0, 0.0,
								1.0, 0.0, 0.0,
								1.0, 1.0, 0.0,
								0.0, 1.0, 0.0};
	const GLfloat TEX_COORDS[] = {0.0, 0.0,
								  1.0, 0.0,
								  1.0, 1.0,
								  0.0, 1.0};

	// copy the vertex data to dynamically allocated memory
	numQuadVertices = 4;
	quadVertices = new GLfloat[numQuadVertices * 3];
	memcpy(quadVertices, VERTICES, sizeof(GLfloat) * numQuadVertices * 3);

	// copy the tex coord data to dynamically allocated memory
	quadTexCoords = new GLfloat[numQuadVertices * 2];
	memcpy(quadTexCoords, TEX_COORDS, sizeof(GLfloat) * numQuadVertices * 2);
}

void renderQuad(GLuint texture)
{
	glEnable(GL_TEXTURE_2D);
	glBindTexture(GL_TEXTURE_2D, texture);

	glEnableClientState(GL_VERTEX_ARRAY);
	glVertexPointer(3, GL_FLOAT, 0, quadVertices);

	glEnableClientState(GL_TEXTURE_COORD_ARRAY);
	glTexCoordPointer(2, GL_FLOAT, 0, quadTexCoords);

	glDisableClientState(GL_COLOR_ARRAY);

	glDrawArrays(GL_QUADS, 0, numQuadVertices);

	glDisable(GL_TEXTURE_2D);
}

// - - - geometry rendering - - - //

void renderSphereSolid()
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, sphereSolidVertices);
	glDisableClientState(GL_COLOR_ARRAY);
    glDrawElements(GL_QUADS, numSphereSolidIndices, GL_UNSIGNED_SHORT, sphereSolidIndices);
}

void renderSpherePoints()
{
    glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, spherePointVertices);
    glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, idListPtr);
    glDrawArrays(GL_POINTS, 0, numSpherePointVertices);

    // advance IDs
    idListPtr += numSpherePointVertices;
}

void renderCubeSolid()
{
	glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, cubeSolidVertices);
	glDisableClientState(GL_COLOR_ARRAY);
    glDrawElements(GL_TRIANGLE_STRIP, numCubeSolidIndices, GL_UNSIGNED_SHORT, cubeSolidIndices);
}

void renderCubePoints()
{
	glEnableClientState(GL_VERTEX_ARRAY);
    glVertexPointer(3, GL_FLOAT, 0, cubePointVertices);
	glEnableClientState(GL_COLOR_ARRAY);
    glColorPointer(4, GL_UNSIGNED_BYTE, 0, idListPtr);
    glDrawArrays(GL_POINTS, 0, numCubePointVertices);

    // advance IDs
    idListPtr += numCubePointVertices;
}

// - - - OpenGL configuration and other low-level stuff is all below - - - //

void openWindow()
{
	const int WIDTH = 800;
	const int HEIGHT = 600;
	const char *TITLE = "Point Rendering with Occlusion";

	int monitorWidth, monitorHeight;
	GLenum error;

	// we need to intialize GLFW before we create a GLFW window
	if(!glfwInit())
	{
		cerr << "openWindow() could not initialize GLFW" << endl;
		exit(1);
	}

	// explicitly set our OpenGL context to something new enough to support the kind of framebuffer functions we need
	glfwWindowHint(GLFW_CONTEXT_VERSION_MAJOR, 3);
	glfwWindowHint(GLFW_CONTEXT_VERSION_MINOR, 0);
	//glfwWindowHint(GLFW_OPENGL_PROFILE, GLFW_OPENGL_COMPAT_PROFILE);
	glfwWindowHint(GLFW_OPENGL_FORWARD_COMPAT, 0);							// allow deprecated functionality
	glfwWindowHint(GLFW_REFRESH_RATE, 60);
	glfwWindowHint(GLFW_RESIZABLE, 0);
	glfwWindowHint(GLFW_DECORATED, 1);
	glfwWindowHint(GLFW_FOCUSED, 1);
	glfwWindowHint(GLFW_OPENGL_DEBUG_CONTEXT, 1);

	// use the current desktop mode to decide on a suitable resolution
	const GLFWvidmode *mode = glfwGetVideoMode(glfwGetPrimaryMonitor());
	monitorWidth = mode -> width;
	monitorHeight = mode -> height;

	// create our OpenGL window using GLFW
    window = glfwCreateWindow(WIDTH, HEIGHT,					// specify width and height
							  TITLE,							// title of window
							  NULL,								// always windowed mode
							  NULL);							// not sharing resources across monitors
	if(!window)
	{
		cerr << "openWindow() could not create an OpenGL window---do your GL context/profile hints make sense?" << endl;
		exit(1);
	}
	glfwMakeContextCurrent(window);
	glfwSetWindowPos(window,									// center the window
					(monitorWidth / 2.0) - (WIDTH / 2),
					(monitorHeight / 2.0) - (HEIGHT / 2));
	glfwSwapInterval(1);

	// configure our viewing area
	glViewport(0, 0, WIDTH, HEIGHT);

	// enable our extensions handler
	glewExperimental = true;		// GLEW bug: glewInit() doesn't get all extensions, so we have it explicitly search for everything it can find
	error = glewInit();
	if(error != GLEW_OK)
	{
		cerr << glewGetErrorString(error) << endl;
		exit(1);
	}

	// clear the OpenGL error code that results from initializing GLEW
	glGetError();

	// save our window size for later on
	windowSize = vec2(WIDTH, HEIGHT);

	// print our OpenGL version info
    cout << "-- GL version:   " << (char*)glGetString(GL_VERSION) << endl;
    cout << "-- GL vendor:    " << (char*)glGetString(GL_VENDOR) << endl;
    cout << "-- GL renderer:  " << (char*)glGetString(GL_RENDERER) << endl;
	cout << "-- GLSL version: " << (char*)glGetString(GL_SHADING_LANGUAGE_VERSION) << endl;

	// turn on OpenGL debugging
	initGLDebugger();
}

void prepareOpenGL()
{
	// turn on depth testing and disable blending
	glEnable(GL_DEPTH_TEST);
	glDisable(GL_BLEND);

	// white screen in background
	glClearColor(1.0, 1.0, 1.0, 1.0);

    // make sure nothing funky goes on---render both front and back of all geometric primitives
	glPolygonMode(GL_FRONT_AND_BACK, GL_FILL);
	glDisable(GL_CULL_FACE);
}

void prepareFramebufferRendering()
{
	glGenFramebuffers(1, &sceneFBO);
	glBindFramebuffer(GL_FRAMEBUFFER, sceneFBO);

	// set up the texture we'll be rendering colour into
	glGenTextures(1, &sceneColorTexture);
	glBindTexture(GL_TEXTURE_2D, sceneColorTexture);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_RGB, windowSize.x, windowSize.y, 0, GL_RGB, GL_UNSIGNED_BYTE, NULL);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);

	// set up the texture we'll be rendering depth into
	glGenTextures(1, &sceneDepthTexture);
	glBindTexture(GL_TEXTURE_2D, sceneDepthTexture);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_NEAREST);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP_TO_EDGE);
	glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP_TO_EDGE);
	glTexImage2D(GL_TEXTURE_2D, 0, GL_DEPTH_COMPONENT32, windowSize.x, windowSize.y, 0, GL_DEPTH_COMPONENT, GL_FLOAT, 0);

	// make sure we've got a depth buffer, too, since we're rendering 3D objects into this framebuffer
	glGenRenderbuffers(1, &sceneDepthBuffer);
	glBindRenderbuffer(GL_RENDERBUFFER, sceneDepthBuffer);
	glRenderbufferStorage(GL_RENDERBUFFER, GL_DEPTH_COMPONENT, windowSize.x, windowSize.y);
	glFramebufferRenderbuffer(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_RENDERBUFFER, sceneDepthBuffer);

	// attach our textures to our framebuffer
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_COLOR_ATTACHMENT0, GL_TEXTURE_2D, sceneColorTexture, 0);
	glFramebufferTexture2D(GL_FRAMEBUFFER, GL_DEPTH_ATTACHMENT, GL_TEXTURE_2D, sceneDepthTexture, 0);
	//GLenum drawBuffers[2] = {GL_COLOR_ATTACHMENT0, GL_DEPTH_ATTACHMENT};
	//glDrawBuffers(2, drawBuffers);
	// for some reason this above line throws a GL callback debugging error in GL 3.0 contexts...

	// make sure everything was set up properly
	if(glCheckFramebufferStatus(GL_FRAMEBUFFER) != GL_FRAMEBUFFER_COMPLETE)
	{
		cerr << "prepareFramebufferRendering(): incomplete FBO: " << glCheckFramebufferStatus(GL_FRAMEBUFFER) << endl;
		exit(1);
	}
}

void prepareViewingParams()
{
	const float FOV = 45.0f;
	const float ASPECT_RATIO = (float)windowSize.x / (float)windowSize.y;

	const float PERSPECTIVE_NEAR_RANGE = 1.0f;
	const float PERSPECTIVE_FAR_RANGE = 100.0f;

	const float STARTING_CAMERA_DISTANCE = 20.0f;
	const float STARTING_CAMERA_ANGLE_X = -M_PI / 4.0;
	const float STARTING_CAMERA_ANGLE_Y = 0.0f;

	// compute our perspective projection---this never changes so we can just do it once
	perspectiveProjection = perspective(FOV, ASPECT_RATIO, PERSPECTIVE_NEAR_RANGE, PERSPECTIVE_FAR_RANGE);

	// compute our ortho projection---this never changes so we can just do it once
	orthoProjection = ortho(0.0f, windowSize.x, 0.0f, windowSize.y);

	// set up our mouse callback that is used to zoom the camera in and out
	glfwSetScrollCallback(window, mouseScrollCallback);

	// position our camera somewhere sane to start our with
	cameraDistance = STARTING_CAMERA_DISTANCE;
	cameraAngleX = STARTING_CAMERA_ANGLE_X;
	cameraAngleY = STARTING_CAMERA_ANGLE_Y;
	setCameraPosition(cameraDistance, cameraAngleX, cameraAngleY);
}

void mouseScrollCallback(GLFWwindow* window, double x, double y)
{
	const float SCROLL_SPEED = 2.0;				// meters we scroll per scroll notch
	const float MIN_DIST = 4.0;					// how close we're allowed to get
	const float MAX_DIST = 40.0;				// how far we're allowed to get

	// adjust the camera distance either forwards or backwards, and keep it within a reasonable distance
	cameraDistance -= (y > 0.0 ? SCROLL_SPEED : -SCROLL_SPEED);
	if(cameraDistance > MAX_DIST) cameraDistance = MAX_DIST;
	if(cameraDistance < MIN_DIST) cameraDistance = MIN_DIST;
}

void moveCameraWithMouse()
{
	const float MAX_ANGLE = (M_PI / 2.0f) - 0.001;
	double mouseX, mouseY;

	// always update our mouse position
	glfwGetCursorPos(window, &mouseX, &mouseY);

	// only do something if we detect a mouse button press
	if(glfwGetMouseButton(window, 0))
	{
		cameraAngleX -= (mouseY - oldMouseY) * 0.01;
		cameraAngleY -= (mouseX - oldMouseX) * 0.01;
		if(cameraAngleX > MAX_ANGLE) cameraAngleX = MAX_ANGLE;
		if(cameraAngleX < -MAX_ANGLE) cameraAngleX = -MAX_ANGLE;
	}

	// track our old mouse position for mouse movement calculations next frame
	oldMouseX = mouseX;
	oldMouseY = mouseY;
}

void setCameraPosition(float distance, float angleX, float angleY)
{
	const vec3 CAMERA_LOOK(0.0, 0.0, 0.0);		// look at center position
	const vec3 CAMERA_UP(0.0, 1.0, 0.0);		// define what direction the camera considers to be 'up'

	// position our camera around the center point based on the incoming distance and viewing angle
	cameraPos = vec3(0.0, 0.0, cameraDistance);
	cameraPos = rotate(cameraPos, angleX, vec3(1.0, 0.0, 0.0));
	cameraPos = rotate(cameraPos, angleY, vec3(0.0, 1.0, 0.0));

	// compute a matrix describing the position and orientation of our camera
	perspectiveView = lookAt(cameraPos, CAMERA_LOOK, CAMERA_UP);

    // use the depreciated GL matrix stack for simplicity here
    glMatrixMode(GL_PROJECTION);
    glLoadIdentity();

    // rather than use GLU, we'll just use what we computed using GLM for our perspective and view matrices
    glLoadMatrixf((GLfloat*)value_ptr(perspectiveProjection));
    glMultMatrixf((GLfloat*)value_ptr(perspectiveView));

    // restore modelview matrix mode and set to the identity
    glMatrixMode(GL_MODELVIEW);
    glLoadIdentity();
}
